# ERON Project

## Tasks

| Progress  |  Task |  Detail | Start date | End date |
|---|---|---|---|---|
| To do | Design Motor mounting for two joints between three motors. | Design | 09-03-22 | 14-03-22 |
| To do |   | Drawing | 14-03-22 | 16-03-22 |
| To do | Design a connector is linked between leg and body that is movable to every part of the body. | Design | 16-03-22 | 21-03-22 |
| To do |   | Drawing | 21-03-22 | 23-03-22 |
| To do | Design a special connector for holding two opposite legs in the same position. | Design | 23-03-22 |  28-03-22 |
| To do |   | Drawing |  28-03-22 | 30-03-22 |


## Equipments

1. [Cabon fiber Tube 3K 24X22X1000 mm](https://www.twinprohobby.com/index.php?page=product&productid=3008&category=16)
2. [Cabon fiber Tube 3K 50X47X1000 mm](https://www.twinprohobby.com/index.php?page=product&productid=3023&category=16)
3. [Carbon fiber filament](https://www.filamentive.com/product-category/carbon-fibre-3d-printer-filament/)
